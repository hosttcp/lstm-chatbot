import codecs
import itertools
import pickle
from random import sample

import nltk
import numpy as np

# EN_WHITELIST = '0123456789abcdefghijklmnopqrstuvwxyz '  # набор символов включенных в белый список
EN_WHITELIST = '0123456789йцукенгшщзхъфывапролджэячсмитьбю '  # набор символов включенных в белый список
EN_BLACKLIST = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~\''  # набор символов включенных в черный список

FILENAME = 'chat.txt'

limit = {
    'max_q': 20,
    'min_q': 0,
    'max_a': 20,
    'min_a': 3
}

UNK = 'unk'
VOCAB_SIZE = 6000


def read_lines(filename):
    """
    Читаем строки из файла
         возвращаем [список строк]
    """
    file_obj = codecs.open(filename, "r", "utf_8_sig")
    text = file_obj.read()  # или читайте по строке
    file_obj.close()

    return text.split('\n')[:-1]


def split_line(line):
    """
    Разделяем предложения одной строки
    на несколько строк
        возвращаем [список строк]
    """
    return line.split('.')


def filter_line(line, whitelist):
    """
    Удаляем все что не входит в словарь
        возращаем строку(чистый ta/en)

    """
    return ''.join([ch for ch in line if ch in whitelist])


def index_(tokenized_sentences, vocab_size):
    """
    Прочитываем список слов, создаём индекс для слова,
    слово для индексирования словоря
        возвращаем кортеж( vocab->(word, count), idx2w, w2idx )

    """
    # Получаем распределение частот
    freq_dist = nltk.FreqDist(itertools.chain(*tokenized_sentences))
    # Получаем словарный запас словаря vocab_size
    vocab = freq_dist.most_common(vocab_size)
    # index2word
    index2word = ['_'] + [UNK] + [x[0] for x in vocab]
    # word2index
    word2index = dict([(w, i) for i, w in enumerate(index2word)])
    return index2word, word2index, freq_dist


def filter_data(sequences):
    """
    Фильтр слишком длинных и слишком коротких предложений
        возвращаем кортеж( filtered_ta, filtered_en )
    """
    filtered_q, filtered_a = [], []
    raw_data_len = len(sequences) // 2

    for i in range(0, len(sequences), 2):
        try:
            q_len, a_len = len(sequences[i].split(' ')), len(sequences[i + 1].split(' '))
            if limit['min_q'] <= q_len <= limit['max_q']:
                if limit['min_a'] <= a_len <= limit['max_a']:
                    filtered_q.append(sequences[i])
                    filtered_a.append(sequences[i + 1])
        except IndexError:
            print('sequences[i + 1] out of range')

    # Показываем размер исходных отфильтрованных данных
    filtered_data_len = len(filtered_q)
    filtered = int((raw_data_len - filtered_data_len) * 100 / raw_data_len)
    print(str(filtered) + '% отфильтрованно из исходных данных')

    return filtered_q, filtered_a


def zero_pad(q_tokenized, a_tokenized, w2idx):
    """
    Создаём финальный набор данных:
    - конвертируем список элементов в массивы индексов
    - добавить нулевое дополнение
          возвращаем ( [array_en([indices]), array_ta([indices]) )
    """
    # Кол-во строк
    data_len = len(q_tokenized)

    # Массивы numpy для хранения индексов
    idx_q = np.zeros([data_len, limit['max_q']], dtype=np.int32)
    idx_a = np.zeros([data_len, limit['max_a']], dtype=np.int32)

    for i in range(data_len):
        q_indices = pad_seq(q_tokenized[i], w2idx, limit['max_q'])
        a_indices = pad_seq(a_tokenized[i], w2idx, limit['max_a'])

        # print(len(idx_q[i]), len(q_indices))
        # print(len(idx_a[i]), len(a_indices))
        idx_q[i] = np.array(q_indices)
        idx_a[i] = np.array(a_indices)

    return idx_q, idx_a


def pad_seq(seq, lookup, max_len):
    """
    Заменить слова на индексы в последовательности
    аменить unknown, если слово не в поиске
        возвращаем [list of indices]
    """
    indices = []
    for word in seq:
        if word in lookup:
            indices.append(lookup[word])
        else:
            indices.append(lookup[UNK])
    return indices + [0] * (max_len - len(seq))


def process_data():
    print('\n>> Чтение строк из файла')
    lines = read_lines(filename=FILENAME)

    # Изменить на нижний регистр (только для en)
    lines = [line.lower() for line in lines]

    print('\n:: Образец из прочитанных (p) строк')
    print(lines[121:125])

    # Отфильтровываем ненужные символы
    print('\n>> Фильтруем строки')
    lines = [filter_line(line, EN_WHITELIST) for line in lines]
    print(lines[121:125])

    # Отфильтровываем слишком длинные или слишком короткие последовательности
    print('\n>> 2ой слой фильтрации')
    q_lines, a_lines = filter_data(lines)
    print('\nq : {0} ; a : {1}'.format(q_lines[60], a_lines[60]))
    print('\nq : {0} ; a : {1}'.format(q_lines[61], a_lines[61]))

    # Преобразуем список [строки текста] в список из [список из слов]
    print('\n>> Сегмент линий в слова')
    q_tokenized = [word_list.split(' ') for word_list in q_lines]
    a_tokenized = [word_list.split(' ') for word_list in a_lines]
    print('\n:: Пример из сегментированного списка слов')
    print('\nq : {0} ; a : {1}'.format(q_tokenized[60], a_tokenized[60]))
    print('\nq : {0} ; a : {1}'.format(q_tokenized[61], a_tokenized[61]))

    # Индексация -> idx2w, w2idx : en/ta
    print('\n >> Индексы слов')
    idx2w, w2idx, freq_dist = index_(q_tokenized + a_tokenized, vocab_size=VOCAB_SIZE)

    print('\n >> Нулевое заполнение')
    idx_q, idx_a = zero_pad(q_tokenized, a_tokenized, w2idx)

    print('\n >> Сохранение массивов numpy на диск')
    # Сохраняем их
    np.save('idx_q.npy', idx_q)
    np.save('idx_a.npy', idx_a)

    # Теперь сохраним необходимые словари
    metadata = {
        'w2idx': w2idx,
        'idx2w': idx2w,
        'limit': limit,
        'freq_dist': freq_dist
    }

    # Записываем на диск: словари для управления данными
    with open('metadata.pkl', 'wb') as f:
        pickle.dump(metadata, f)


def load_data(path=''):
    # Читаем словари для управления данными
    try:
        with open(path + 'metadata.pkl', 'rb') as f:
            metadata = pickle.load(f)
    except:
        metadata = None
    # Считываем numpy массив
    idx_q = np.load(path + 'idx_q.npy')
    idx_a = np.load(path + 'idx_a.npy')
    return metadata, idx_q, idx_a


def split_data_set(x, y, ratio=None):
    """
    Разделять данные на порции (70%), тест (15%) и валидация (15%)
        возвращаем кортеж( (train_x, train_y), (test_x,test_y), (valid_x,valid_y) )
    """

    if ratio is None:
        ratio = [0.7, 0.15, 0.15]

    # Количество примеров
    data_len = len(x)
    lens = [int(data_len * item) for item in ratio]

    train_x, train_y = x[:lens[0]], y[:lens[0]]
    test_x, test_y = x[lens[0]:lens[0] + lens[1]], y[lens[0]:lens[0] + lens[1]]
    valid_x, valid_y = x[-lens[-1]:], y[-lens[-1]:]

    return (train_x, train_y), (test_x, test_y), (valid_x, valid_y)


def batch_gen(x, y, batch_size):
    """
    Создаём партии из набора данных
        yield (x_gen, y_gen)
        TODO : fix needed
    """
    # Бесконечность
    while True:
        for i in range(0, len(x), batch_size):
            if (i + 1) * batch_size < len(x):
                yield x[i: (i + 1) * batch_size].T, y[i: (i + 1) * batch_size].T


def rand_batch_gen(x, y, batch_size):
    """
    Создаёём партии, путем случайной выборки из кучи айтемов
        yield (x_gen, y_gen)
    """
    while True:
        sample_idx = sample(list(np.arange(len(x))), batch_size)
        yield x[sample_idx].T, y[sample_idx].T


def decode(sequence, lookup, separator=''):  # 0 используется для заполнения, игнорируем
    """
    Общая функция декодирования
        Вход: последовательность, поиск
    """
    return separator.join([lookup[element] for element in sequence if element])


# def decode_word(alpha_seq, idx2alpha):
#     """
#     Конвертируем индексы из алфавита в строку (слово)
#        возвращаем str(word)
#     """
#     return ''.join([idx2alpha[alpha] for alpha in alpha_seq if alpha])
#
#
# def decode_phonemes(pho_seq, idx2pho):
#     """
#     Конвертируем индексы фонем в список фонем (как строку)
#        возвращаем str(phoneme_list)(список фонем)
#     """
#     return ' '.join([idx2pho[pho] for pho in pho_seq if pho])


if __name__ == '__main__':
    process_data()
