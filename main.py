import click
import tensorflow as tf
import tensorlayer as tl

from tqdm import tqdm
from sklearn.utils import shuffle
from tensorlayer.layers import DenseLayer, EmbeddingInputlayer, Seq2Seq, retrieve_seq_length_op2

from data.twitter import data

sess_config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)


@click.command()
@click.option('-dc', '--data-corpus', default='dota', help='Корпус данных для обучения и вывода', )
@click.option('-bs', '--batch-size', default=32, help='Размер партии для обучения на мини-выборке', )
@click.option('-n', '--num-epochs', default=50, help='Количество эпох для обучения', )
@click.option('-lr', '--learning-rate', default=0.001,
              help='Уровень обучения, который следует использовать при обучении модели', )
@click.option('-inf', '--inference-mode', is_flag=True, help='Флаг для режима INFERENCE', )
def train(data_corpus, batch_size, num_epochs, learning_rate, inference_mode):
    metadata, train_x, train_y, test_x, test_y, valid_x, valid_y = initial_setup(data_corpus)

    # Параметры
    src_len = len(train_x)
    tgt_len = len(train_y)

    assert src_len == tgt_len

    n_step = src_len // batch_size
    src_vocab_size = len(metadata['idx2w'])  # 8002 (0~8001)
    emb_dim = 1024

    word2idx = metadata['w2idx']  # Библиотека слов 2 индекса
    idx2word = metadata['idx2w']  # Список индексов 2 слова

    unk_id = word2idx['unk']  # 1
    pad_id = word2idx['_']  # 0

    start_id = src_vocab_size  # 8002
    end_id = src_vocab_size + 1  # 8003

    word2idx.update({'start_id': start_id})
    word2idx.update({'end_id': end_id})
    idx2word = idx2word + ['start_id', 'end_id']

    src_vocab_size = tgt_vocab_size = src_vocab_size + 2

    """ Данные для Seq2Seq модели должны выглядеть так:
    input_seqs : ['how', 'are', 'you', '<PAD_ID'>]
    decode_seqs : ['<START_ID>', 'I', 'am', 'fine', '<PAD_ID'>]
    target_seqs : ['I', 'am', 'fine', '<END_ID>', '<PAD_ID'>]
    target_mask : [1, 1, 1, 1, 0]
    """
    # Пропрецессинг
    target_seqs = tl.prepro.sequences_add_end_id([train_y[10]], end_id=end_id)[0]
    decode_seqs = tl.prepro.sequences_add_start_id([train_y[10]], start_id=start_id, remove_last=False)[0]
    target_mask = tl.prepro.sequences_get_mask([target_seqs])[0]
    if not inference_mode:
        print("encode_seqs", [idx2word[ids] for ids in train_x[10]])
        print("target_seqs", [idx2word[ids] for ids in target_seqs])
        print("decode_seqs", [idx2word[ids] for ids in decode_seqs])
        print("target_mask", target_mask)
        print(len(target_seqs), len(decode_seqs), len(target_mask))

    # Инициализируем сессию TensorFlow
    tf.reset_default_graph()
    sess = tf.Session(config=sess_config)

    # Данные для обучения
    encode_seqs = tf.placeholder(dtype=tf.int64, shape=[batch_size, None], name="encode_seqs")
    decode_seqs = tf.placeholder(dtype=tf.int64, shape=[batch_size, None], name="decode_seqs")
    target_seqs = tf.placeholder(dtype=tf.int64, shape=[batch_size, None], name="target_seqs")
    target_mask = tf.placeholder(dtype=tf.int64, shape=[batch_size, None], name="target_mask")

    net_out, _ = create_model(encode_seqs, decode_seqs, src_vocab_size, emb_dim, is_train=True, reuse=False)
    net_out.print_params(False)

    # Данные для вывода
    encode_seqs2 = tf.placeholder(dtype=tf.int64, shape=[1, None], name="encode_seqs")
    decode_seqs2 = tf.placeholder(dtype=tf.int64, shape=[1, None], name="decode_seqs")

    net, net_rnn = create_model(encode_seqs2, decode_seqs2, src_vocab_size, emb_dim, is_train=False, reuse=True)
    y = tf.nn.softmax(net.outputs)

    # Функция потерь
    loss = tl.cost.cross_entropy_seq_with_mask(logits=net_out.outputs, target_seqs=target_seqs,
                                               input_mask=target_mask, return_details=False, name='cost')

    # Оптимизатор
    train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)

    # Инициализация переменных
    sess.run(tf.global_variables_initializer())

    # Загружаем модель
    tl.files.load_and_assign_npz(sess=sess, name='model.npz', network=net)

    """
    Вывод с использованием предварительно подготовленной модели
    """

    def inference(query):
        seed_id = [word2idx.get(w, unk_id) for w in query.split(" ")]

        # Кодируем и получаем состояние
        state = sess.run(net_rnn.final_state_encode,
                         {encode_seqs2: [seed_id]})
        # Декодируем, передаём start_id и получаем первое слово
        o, state = sess.run([y, net_rnn.final_state_decode],
                            {net_rnn.initial_state_decode: state,
                             decode_seqs2: [[start_id]]})
        w_id = tl.nlp.sample_top(o[0], top_k=3)
        w = idx2word[w_id]
        # Состояние декодирования и передача итеративно
        max_length_sentence = [w]
        for _ in range(30):  # максимальная длина предложения
            o, state = sess.run([y, net_rnn.final_state_decode],
                                {net_rnn.initial_state_decode: state,
                                 decode_seqs2: [[w_id]]})
            w_id = tl.nlp.sample_top(o[0], top_k=2)
            w = idx2word[w_id]
            if w_id == end_id:
                break
            max_length_sentence = max_length_sentence + [w]
        return max_length_sentence

    if inference_mode:
        print('Режим тестирвоания')
        print('--------------')
        while True:
            input_seq = input('Запрос: ')
            sentence = inference(input_seq)
            print(" >", ' '.join(sentence))
    else:
        seeds = ["почему асфальт чёрный",
                 "где на тратуаре поребрик и где мои носки, карл"]
        for epoch in range(num_epochs):
            train_x, train_y = shuffle(train_x, train_y, random_state=0)
            total_loss, n_iter = 0, 0
            for x, Y in tqdm(
                    tl.iterate.minibatches(inputs=train_x, targets=train_y, batch_size=batch_size, shuffle=False),
                    total=n_step, desc='Epoch[{}/{}]'.format(epoch + 1, num_epochs), leave=False):
                x = tl.prepro.pad_sequences(x)
                _target_seqs = tl.prepro.sequences_add_end_id(Y, end_id=end_id)
                _target_seqs = tl.prepro.pad_sequences(_target_seqs)
                _decode_seqs = tl.prepro.sequences_add_start_id(Y, start_id=start_id, remove_last=False)
                _decode_seqs = tl.prepro.pad_sequences(_decode_seqs)
                _target_mask = tl.prepro.sequences_get_mask(_target_seqs)
                # Здесь данные в сыром формате
                # for i in range(len(x)):
                #     print(i, [idx2word[ids] for ids in x[i]])
                #     print(i, [idx2word[ids] for ids in Y[i]])
                #     print(i, [idx2word[ids] for ids in _target_seqs[i]])
                #     print(i, [idx2word[ids] for ids in _decode_seqs[i]])
                #     print(i, _target_mask[i])
                #     print(len(_target_seqs[i]), len(_decode_seqs[i]), len(_target_mask[i]))
                _, loss_iter = sess.run([train_op, loss], {encode_seqs: x, decode_seqs: _decode_seqs,
                                                           target_seqs: _target_seqs, target_mask: _target_mask})
                total_loss += loss_iter
                n_iter += 1

            # Выводим среднее значение потери после каждой эпохи
            print('Epoch [{}/{}]: loss {:.4f}'.format(epoch + 1, num_epochs, total_loss / n_iter))

            # Вывод после каждой эпохи
            for seed in seeds:
                print("Запрос >", seed)
                for _ in range(5):
                    sentence = inference(seed)
                    print(" >", ' '.join(sentence))

            # Сохранение модели
            tl.files.save_npz(net.all_params, name='model.npz', sess=sess)

    # Очищаем сессию
    sess.close()


"""
Создание модели LSTM
"""


def create_model(encode_seqs, decode_seqs, src_vocab_size, emb_dim, is_train=True, reuse=False):
    with tf.variable_scope("model", reuse=reuse):
        # для чат бота можно использовать один и тот же уровень внедрения
        # для перевода можно использовать 2 отдельных слоя внедрения
        with tf.variable_scope("embedding") as vs:
            net_encode = EmbeddingInputlayer(
                inputs=encode_seqs,
                vocabulary_size=src_vocab_size,
                embedding_size=emb_dim,
                name='seq_embedding')
            vs.reuse_variables()
            net_decode = EmbeddingInputlayer(
                inputs=decode_seqs,
                vocabulary_size=src_vocab_size,
                embedding_size=emb_dim,
                name='seq_embedding')

        net_rnn = Seq2Seq(net_encode, net_decode,
                          cell_fn=tf.nn.rnn_cell.LSTMCell,
                          n_hidden=emb_dim,
                          initializer=tf.random_uniform_initializer(-0.1, 0.1),
                          encode_sequence_length=retrieve_seq_length_op2(encode_seqs),
                          decode_sequence_length=retrieve_seq_length_op2(decode_seqs),
                          initial_state_encode=None,
                          dropout=(0.5 if is_train else None),
                          n_layer=3,
                          return_seq_2d=True,
                          name='seq2seq')

        net_out = DenseLayer(net_rnn, n_units=src_vocab_size, act=tf.identity, name='output')
    return net_out, net_rnn


"""
Инициализация настроек
"""


def initial_setup(data_corpus):
    metadata, idx_q, idx_a = data.load_data(path='data/{}/'.format(data_corpus))
    (train_x, train_y), (test_x, test_y), (valid_x, valid_y) = data.split_data_set(idx_q, idx_a)
    train_x = tl.prepro.remove_pad_sequences(train_x.tolist())
    train_y = tl.prepro.remove_pad_sequences(train_y.tolist())
    test_x = tl.prepro.remove_pad_sequences(test_x.tolist())
    test_y = tl.prepro.remove_pad_sequences(test_y.tolist())
    valid_x = tl.prepro.remove_pad_sequences(valid_x.tolist())
    valid_y = tl.prepro.remove_pad_sequences(valid_y.tolist())
    return metadata, train_x, train_y, test_x, test_y, valid_x, valid_y


def main():
    try:
        train()
    except KeyboardInterrupt:
        print('Отмена!')


if __name__ == '__main__':
    main()
